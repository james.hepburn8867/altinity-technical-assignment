from testflows.core import *
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.service import Service as FFS
from selenium.webdriver.support.ui import WebDriverWait as WDW
from selenium.webdriver.common.by import By

# test the title of the header section of Altinity's BLOG 

def test_altinity_blog ():
    
    # sets up the firefox service and the web driver

    with FFS (executable_path = '/path/to/geckodriver') as service:
        with webdriver.Firefox (service = service) as driver:

            # opens the website

            driver.get ('https://altinity.com/')

            # navigates to the BLOG section

            button = WDW (driver, 10).until (EC.presence_of_element_located (By.LINK_TEXT, 'BLOG'))
            button.click()

            # assertion for the title of the header section

            title = WDW (driver, 10).until (EC.presence_of_element_located (By.CSS_SELECTOR, 'header.blog-page-header h1'))
            assert title.text == 'Altinity Blog'
