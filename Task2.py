import subprocess, testflows.asserts

# tests the clickhouse-client query SELECT 1

def test_clickhouse_client_select_1 ():
    
    # runs the --query with SELECT 1 
    output = subprocess.run (["clickhouse-client", "--query", "SELECT 1"], stdout = subprocess.PIPE)
    
    # assets that the output is 1

    testflows.asserts.equal (int (output.stdout.decode ('utf-8')), 1)
