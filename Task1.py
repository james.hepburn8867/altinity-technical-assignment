import os, subprocess

# tests -a option in ls
# -a shows all files in a directory, including the hidden ones

def test_ls_a_option ():

    # creates a temporary directory with a few files

    os.makedirs ("temp_dir") # makes a directory with the name "temp_dir"
    os.chdir ("temp_dir") # changes the current directory to the temporary directory

    # makes two text files and two hidden files

    open ("file1.txt", "w").close ()
    open ("file2.txt", "w").close ()
    open (".hidden1", "w").close ()
    open (".hidden2", "w").close ()

    # runs the -a option, converts it into a string and splits it into a list

    output = subprocess.check_output (["ls", "-a"]).decode ().splitlines ()

    # verify the directory assertions

    assert "." in output # assertion for current directory
    assert ".." in output # assertion for parrent directory

    # verify the file assertions

    assert "file1.txt" in output 
    assert "file2.txt" in output 
    assert ".hidden1" in output 
    assert ".hidden2" in output 

    # clean up 

    os.chdir ("..") # changes the current directory to the parent directory
    subprocess.run (["rm", "-rf", "temp_dir"]) # deletes test directory and its content

# tests -all option in ls
# --all shows all files in a directory, including the hidden ones
# --all is equivalent to -a

def test_ls_all_option ():

    # creates a temporary directory with a few files

    os.makedirs ("temp_dir") # makes a directory with the name "temp_dir"
    os.chdir ("temp_dir") # changes the current directory to the temporary directory

    # makes two text files and two hidden files

    open ("file1.txt", "w").close ()
    open ("file2.txt", "w").close ()
    open (".hidden1", "w").close ()
    open (".hidden2", "w").close ()

    # runs the --all option, converts it into a string and splits it into a list

    output = subprocess.check_output (["ls", "--all"]).decode ().splitlines ()

    # verify the directory assertions

    assert "." in output # assertion for current directory
    assert ".." in output # assertion for parrent directory

    # verify the file assertions

    assert "file1.txt" in output 
    assert "file2.txt" in output 
    assert ".hidden1" in output 
    assert ".hidden2" in output 

    # clean up 

    os.chdir ("..") # changes the current directory to the parent directory
    subprocess.run (["rm", "-rf", "temp_dir"]) # deletes test directory and its content

# tests --author option in ls
# --author adds the name of the author of a file or a directory to the output

def test_ls_author_option ():

    # creates a temporary directory with a few files

    os.makedirs ("temp_dir") # makes a directory with the name "temp_dir"
    os.chdir ("temp_dir") # changes the current directory to the temporary directory

    # makes a text file

    open ("file.txt", "w").close ()

    # runs the --author option, converts it into a string and splits it into a list

    output = subprocess.check_output (["ls", "--author", "file.txt"]).decode ().splitlines ()

    # assertion for author and output

    assert "root" in output [0]

    # clean up 

    os.chdir ("..") # changes the current directory to the parent directory
    subprocess.run (["rm", "-rf", "temp_dir"]) # deletes test directory and its content
